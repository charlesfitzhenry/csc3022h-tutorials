CC=g++
CCFLAGS=-std=c++11
SOURCES=driver.cpp database.cpp helpers.cpp
OBJECTS=driver.o database.o helpers.o

driver: $(OBJECTS)
	$(CC) $(CCFLAGS) $(OBJECTS) -o driver
	
.cpp.o:
	$(CC) $(CCFLAGS) -c $<
	
clean:
	rm *.o driver

run:
	./driver
	
add:
	git add *.cpp *.h Makefile
