#include "database.h"
#include <cstdlib>
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;
using namespace FTZCHA002;
int main(){
		
		char i = {0};
		
		for(;;){
			menu();
		cin >> i;
		
		if(i=='q' || i=='Q'){
			//Exit program
			clear();
			cout << "Bye now..." << endl;
			break;
		}
		
		else {
			clear();
	
			switch(i){
			case '1':	
			addStudent();
			break;
			
			case '2':
			readDB();
			break;
			
			case '3':
			saveDB();
			break;
			
			case '4':
			displayStudentData();
			break;
			
			case '5':
			gradeStudent();
			break;
		
			default:
			cout << "That was not a valid option! Try again." << endl;
		}
		cout << "---------------------------------------" << endl;
	}
	
	}
		
}

