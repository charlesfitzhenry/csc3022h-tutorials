#include <cstdlib>
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include "helpers.h"

using namespace std;
namespace FTZCHA002{
struct StudentRecord{
	std::string name;
	std::string surname;
	std::string studentNumber;
	std::string classRecord;
	
	StudentRecord(std::string n, std::string sn, std::string stdn, std::string cr){
		name = n;
		surname = sn;
		studentNumber = stdn;
		classRecord = cr;
	}
};

std::vector<StudentRecord> students;

void menu(){
	
	cout << "1: Add student\n";
	cout << "2: Read database\n";
	cout << "3: Save database\n";
	cout << "4: Display given student data\n";
	cout << "5: Grade student\n";
	cout << "q: Quit\n\n";
	cout << "Enter a number (or q to quit) and press return..." << endl;
}

//Function definitions
int addStudent(){
	string tempN;
	string tempSn;
	string tempStdn;
	string tempC;
	
	cout << "function addStudent() called" << endl;
	
	//Flush cin to make it happy :) and to correctly accept the inputs which follow
	cin.ignore();
	
	cout << "Enter student name:" << endl;
	// get line from cin
	getline(cin, tempN);
	
	cout << "Enter student surname:" << endl;
	// get line from cin
	getline(cin, tempSn);
	
	cout << "Enter student number:" << endl;
	// get line from cin
	getline(cin, tempStdn);
	
	cout << "Enter student class record:" << endl;
	// get line from cin
	getline(cin, tempC);

	//Convert all input to upper case
	tempN = toUpper(tempN);
	tempSn = toUpper(tempSn);
	tempStdn = toUpper(tempStdn);
	
	//Check if student already exists by using student number as primary key
if(!students.empty()){
	for (auto it = begin (students); it != end (students); ++it) {
		
		//Check if student number exists
		if((it->studentNumber).compare(tempStdn) == 0){

			//Student number exists, modify data
			it->name = tempN;
			it->surname = tempSn;
			it->classRecord = tempC;
			break;
		}	
		else{	
			//Student number does not exist in db yet
			//Add new student record to vector
			students.push_back(StudentRecord(tempN, tempSn, tempStdn, tempC));
			break;
		}
}
return 0;

} else{
	students.push_back(StudentRecord(tempN, tempSn, tempStdn, tempC));
	return 0;
}}

int readDB(){
	string filename = "database.txt";

	// Open file with a file stream. ifstream constructor
	// wants a C char * string, so call c_str() method.
	ifstream in(filename.c_str(), ifstream::in);

	if(!in)
		{ cout << "Couldn't open " << filename << endl; return 1;}
		else{
			string record;
			
			size_t pos;
			string rName;
			string rSname;
			string rSnum;
			string rGrades;
			
			string temp;
			
			getline(in, record);
			
			//Initialise vector by clearing out all current data
			students.clear();
			
			while(in.good() && record!=""){		
						
				
				//Extract name
				pos = record.find(", ");
				rName = record.substr(0,pos);
				record = record.substr(pos+2,record.length());
			
				//Extract surname
				pos = record.find(", ");
				rSname = record.substr(0,pos);
				record = record.substr(pos+2, record.length());
				
				//Extract student number
				pos = record.find(", ");
				rSnum = record.substr(0,pos);
				record = record.substr(pos+2, record.length());
				
				//Extract grades
				pos = record.find(", ");
				rGrades = record.substr(0,pos);
				record = record.substr(pos+2, record.length());
				
				//Push populated struct into initialised vector
				
				students.push_back(StudentRecord(rName, rSname, rSnum, rGrades));
			
			getline(in, record);
			}
		}

	// Close file
	in.close();
	
	cout << "Database successfully read" << endl;
	return 0;	
}

int saveDB(){
	//Check if there is data in vector to save
	if(!students.empty()){
	
	string filename = "database.txt";

	// Open file with a file stream. ifstream constructor
	// wants a C char * string, so call c_str() method.
	ofstream out(filename.c_str(), ofstream::trunc); //trunc to clear file first

	if(!out)
		{ cout << "Couldn't open " << filename << endl; return 1; }
		else{
			//write to file
			for (auto it = begin (students); it != end (students); ++it) {
				out << it->name << ", " << it->surname << ", " << it->studentNumber << ", " << it->classRecord << "\n";
			  }
			  
		  //close file
		  out.close();
		  
		  cout << "Database saved" << endl;
		  return 0;
	  }
  }
  else{
	  //If the vector is empty. ie no data
	  cout << "No data to save. First add student data" << endl;
	  return 1;
  }
}

int displayStudentData(){
	if(!students.empty()){
		
	string studentNumber;
	
	//Flush cin to make it happy :) and to correctly accept the inputs which follow
	cin.ignore();
	
	cout << "Enter student number:" << endl;
	// get line from cin
	getline(cin, studentNumber);
		
	for (auto it = begin (students); it != end (students); ++it) {
		
		if((it->studentNumber).compare(toUpper(studentNumber)) == 0){
		cout << "------------------------------" << endl;
		cout << "Student Name:    " << it->name << endl;
		cout << "Student Surname: " << it->surname << endl;
		cout << "Student Number:  " << it->studentNumber << endl;
		cout << "Class record:    " << it->classRecord << endl;
		cout << "------------------------------" << endl;
		return 0;	
		break;
}
else{
	cout << "No record was found for student number " << studentNumber << endl;
	}

}
return 0;
} else{
		cout << "Database is empty" << endl;
		return 1;	
	
}
}

int gradeStudent(){
	if(!students.empty()){
		
	string studentNumber;
	string nameCombo;
	string classRec;
	int total = 0;
	int entries = 0;
	int finalAvg = 0;
	size_t pos;
	
	//Flush cin to make it happy :) and to correctly accept the inputs which follow
	cin.ignore();
	
	cout << "Enter student number:" << endl;
	// get line from cin
	getline(cin, studentNumber);
	
	//Convert student number to upper case
	//boost::to_upper(studentNumber);
	//string newstr(boost::to_upper_copy<string>(studentNumber));
	//studentNumber = newstr;
		
	for (auto it = begin (students); it != end (students); ++it) {
		
		if((it->studentNumber).compare(studentNumber) == 0){

		nameCombo = it->name + " " + it->surname + " (" + it->studentNumber + ")";
		classRec = it->classRecord;
		if(!classRec.empty()){
		//Calcuate average
		while(pos!=string::npos){
			pos = classRec.find(" ");
			
			//If more than one entry
			if(pos!=string::npos){
			total += stoi(classRec.substr(0, pos));
			classRec = classRec.substr(pos +1, classRec.length());
			}
			else{
			total += stoi(classRec);	
			}
			entries++;
		}
		
		//Average
		finalAvg = (total/entries);
		
		//Print it out
		cout << "The average for " << nameCombo << ", is " << finalAvg << endl;
		
		return 0;
		break;
	}
	else{
		//No class records for the student
		cout << "There are no class records for " << nameCombo << endl;
		return 1;
		break;
	}
		
		
}
else{
	cout << "No record was found for student number " << studentNumber << endl;
	}

}
return 0;
} else{
		cout << "Database is empty" << endl;
		return 1;	
	
}
}

void clear(void){
	//Clears the terminal
	system("clear");
}

}
