#ifndef DATABASE_H
#define DATABASE_H
#include <sstream>
#include <vector>
namespace FTZCHA002{
//Method declarations
int addStudent();
int readDB();
int saveDB();
int displayStudentData();
int gradeStudent();
void clear(void);
void menu();
}
#endif

