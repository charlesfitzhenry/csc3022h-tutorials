#include "header.h"
#include <cstdlib>
#include <sstream>
#include <vector>
#include <fstream>

namespace FTZCHA002{
using namespace std;
void menu(){
	
	cout << "1: Add student\n";
	cout << "2: Read database\n";
	cout << "3: Save database\n";
	cout << "4: Display given student data\n";
	cout << "5: Grade student\n";
	cout << "q: Quit\n\n";
	cout << "Enter a number (or q to quit) and press return..." << endl;
	
}

int main(){
		
		char i = {0};
		
		for(;;){
			menu();
		cin >> i;
		
		if(i=='q' || i=='Q'){
			//Exit program
			clear();
			cout << "Bye now..." << endl;
			break;
		}
		
		else {
			clear();
	
			switch(i){
			case '1':	
			addStudent();
			break;
			
			case '2':
			readDB();
			break;
			
			case '3':
			saveDB();
			break;
			
			case '4':
			displayStudentData();
			break;
			
			case '5':
			gradeStudent();
			break;
		
			default:
			cout << "That was not a valid option! Try again." << endl;
		}
		cout << "---------------------------------------" << endl;
	}
	
	}
		vector<StudentRecord> students;
}
}
